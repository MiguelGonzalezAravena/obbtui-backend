'use strict';
var reply = require('../../base/utils/reply');

function getPeriodo(request, response) {
  try {
    // Obtener año y periodo del estudiante
    let resp = {
      anio: 2018,
      semestre: 5
    };

    response.json(reply.ok(resp));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

module.exports = {
  getPeriodo
};